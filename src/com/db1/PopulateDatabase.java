/*
 * @Authors 
 * 
 * Prajwal Prasad - 1001750483
 * Rajath Muthappa Kallichanda - 1001724662
 * 
 */

package com.db1;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;

public class PopulateDatabase {
	static final String DB_URL = "jdbc:oracle:thin:@(DESCRIPTION=(ADDRESS_LIST=(ADDRESS=(PROTOCOL=TCP)(HOST=acaddbprod-1.uta.edu)(PORT=1523)))(CONNECT_DATA=(SERVICE_NAME=pcse1p.data.uta.edu)))";

	static final String USER = "rxm4662";
	static final String PASS = "Rajath01121992";

	public static void main(String[] args) {
		Connection conn = null;
		Statement stmt = null;
		try {
			Class.forName("oracle.jdbc.OracleDriver");

			System.out.println("Connecting to database...");
			conn = DriverManager.getConnection(DB_URL, USER, PASS);
			System.out.println("Connection successfull...");
			populateEmployeeTable(conn, stmt);
			populateDepartmentTable(conn, stmt);
			populateDepartmentLocationTable(conn, stmt);
			populateProjectTable(conn, stmt);
			populateWorks_OnTable(conn, stmt);
			populateDependentTable(conn, stmt);
			updateEmployeeTable(conn, stmt);

		} catch (SQLException se) {
			se.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				if (conn != null)
					conn.close();
			} catch (SQLException se) {
				se.printStackTrace();
			}
		}
		System.out.println("ALL DONE!");
	}

	private static void updateEmployeeTable(Connection conn, Statement stmt) throws IOException, SQLException {
		// TODO Auto-generated method stub

		FileReader fr = new FileReader(new File("./DataFiles/EMPLOYEE.txt")); // reads the file
		BufferedReader br = new BufferedReader(fr); // creates a buffering character input stream
		String line;
		while ((line = br.readLine()) != null) {
			String[] data = line.split(", ");
			stmt = conn.createStatement();

			String sql = "UPDATE EMPLOYEE SET DNO = " + data[9] + " where SSN =  " + data[3];
			stmt.executeUpdate(sql);
		}
		fr.close();
	}

	private static void populateDependentTable(Connection conn, Statement stmt) throws IOException, SQLException {
		FileReader fr = new FileReader(new File("./DataFiles/DEPENDENT.txt")); // reads the file
		BufferedReader br = new BufferedReader(fr); // creates a buffering character input stream
		String line;
		while ((line = br.readLine()) != null) {
			String[] data = line.split(", ");
			stmt = conn.createStatement();
			String sql = "INSERT INTO DEPENDENT VALUES (" + data[0] + ", " + data[1] + ", " + data[2] + ", " + data[3]
					+ ", " + data[4] + ")";
			stmt.executeUpdate(sql);
		}
		fr.close();

	}

	private static void populateWorks_OnTable(Connection conn, Statement stmt) throws IOException, SQLException {
		FileReader fr = new FileReader(new File("./DataFiles/WORKS_ON.txt")); // reads the file
		BufferedReader br = new BufferedReader(fr); // creates a buffering character input stream
		String line;
		while ((line = br.readLine()) != null) {
			String[] data = line.split(", ");
			stmt = conn.createStatement();
			String sql = "INSERT INTO WORKS_ON VALUES (" + data[0] + ", " + data[1] + ", " + data[2] + ")";
			stmt.executeUpdate(sql);
		}
		fr.close();

	}

	private static void populateProjectTable(Connection conn, Statement stmt) throws IOException, SQLException {
		FileReader fr = new FileReader(new File("./DataFiles/PROJECT.txt")); // reads the file
		BufferedReader br = new BufferedReader(fr); // creates a buffering character input stream
		String line;
		while ((line = br.readLine()) != null) {
			String[] data = line.split(", ");
			stmt = conn.createStatement();
			String sql = "INSERT INTO PROJECT VALUES (" + data[0] + ", " + data[1] + ", " + data[2] + ", " + data[3]
					+ ")";
			stmt.executeUpdate(sql);
		}
		fr.close();

	}

	private static void populateDepartmentLocationTable(Connection conn, Statement stmt)
			throws IOException, SQLException {
		FileReader fr = new FileReader(new File("./DataFiles/DEPT_LOCATIONS.txt")); // reads the file
		BufferedReader br = new BufferedReader(fr); // creates a buffering character input stream
		String line;
		while ((line = br.readLine()) != null) {
			String[] data = line.split(", ");
			stmt = conn.createStatement();

			String sql = "INSERT INTO DEPT_LOCATIONS VALUES (" + data[0] + ", " + data[1] + ")";
			stmt.executeUpdate(sql);
		}
		fr.close();

	}

	private static void populateDepartmentTable(Connection conn, Statement stmt) throws IOException, SQLException {
		FileReader fr = new FileReader(new File("./DataFiles/DEPARTMENT.txt")); // reads the file
		BufferedReader br = new BufferedReader(fr); // creates a buffering character input stream
		String line;
		while ((line = br.readLine()) != null) {
			String[] data = line.split(", ");
			stmt = conn.createStatement();

			String sql = "INSERT INTO DEPARTMENT VALUES (" + data[0] + ", " + data[1] + ", " + data[2] + ", " + data[3]
					+ ")";
			stmt.executeUpdate(sql);
		}
		fr.close();

	}

	private static void populateEmployeeTable(Connection conn, Statement stmt) throws IOException, SQLException {
		FileReader fr = new FileReader(new File("./DataFiles/EMPLOYEE.txt")); // reads the file
		BufferedReader br = new BufferedReader(fr); // creates a buffering character input stream
		String line;
		while ((line = br.readLine()) != null) {
			String[] data = line.split(", ");
			stmt = conn.createStatement();

			String sql = "INSERT INTO EMPLOYEE VALUES (" + data[0] + ", " + data[1] + ", " + data[2] + ", " + data[3]
					+ " , " + data[4] + ", " + data[5] + ", " + data[6] + ", " + data[7] + ", " + data[8] + ", 0)";
			stmt.executeUpdate(sql);
		}
		fr.close();

	}
}
